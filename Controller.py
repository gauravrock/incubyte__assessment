'''
Filename: Incubyte\Controller.py
Created Date: Thursday, July 8th 2021, 9:17:51 pm
Author: Gaurav sharma
Copyright (c) 2021 Incubyte

Main Controller Template to execute the ETL Pipeline and managing other module instances and args.
############################################ ETL PIPELINE #######################################
                Extraction -----> Transformaation -----> Loading
Requirement to execute ETL: 1.setting a postgres DB with 'Incubyte' database and schema as 'demo'.
'''

from Utils import Util
import sys
import Logger
import Constants

if __name__ == "__main__":
    # Instantiate the logger
    Constants.logger = Logger.setup_custom_logger()
    #------------ MAIN --------------#
    if len(sys.argv) != 3:
        raise Exception('This Script requires 2 arguments, pls look at arguments supplied:\n'
                        '1.Sample Data File\n'
                        '2.Detail Records')

    Constants.filepath = sys.argv[1]
    Constants.details_filepath = sys.argv[2]

    try:
        # Connection to Db-postgres
        Util.connection()

        # 1. Extraction of data
        Util.extraction(Constants.filepath, Constants.details_filepath)

        # 2.transforming data
        Util.transformation()

        # 3.loading data to stg and ref
        Util.loading()

        Constants.logger.info(
            '################################################ ETL PIPELINE SUCCESS################################################')
    except Exception as e:
        print(e)
        Constants.logger.info(
            '################################################ETL PIPELINE FAILED################################################')
