import pandas as pd
import Constants
from configparser import ConfigParser
import psycopg2
import psycopg2.extras as extras
import numpy as np

'''
Utils class covers all the Connections to Popstgres (OLTP DB used in this assessment).
Contains all the majot steps for the ETL pipeline:
1. Extraction
2.Transformation
3.Loading
'''


class Util:
    '''
    utility module for all the function performed by the ETL pipeline
    '''
    @staticmethod
    def config(filename='database.ini', section='postgresql'):
        # create a parser
        parser = ConfigParser()
        # read config file
        parser.read(filename)
        # get section, default to postgresql
        db = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                db[param[0]] = param[1]
        else:
            raise Exception(
                'Section {0} not found in the {1} file'.format(section, filename))
        return db

    @staticmethod
    def connection():
        '''
        setting connections with database using psycopg2
        '''
        conn = None
        # read connection parameters
        params = Util.config()
        # connect to the PostgreSQL server
        Constants.logger.info('Connecting to the database...')
        conn = psycopg2.connect(**params)
        conn.autocommit = True
        # create a cursor
        Constants.curs = conn.cursor()
        Constants.logger.info('Connected Successfully...')

    @staticmethod
    def extraction(filepath, details_filepath):
        '''
        reads objects from source system file, currently only supports txt and extract all necessary information
        '''
        stg_data = pd.read_csv(filepath, delimiter="|")
        details_record = pd.read_csv(details_filepath, delimiter=" ")

        Constants.logger.info(
            f'####################Starting Etl process for {Constants.filepath}######################')
        # dropping first column as it says about headers and domains data
        Constants.stg_data = stg_data.iloc[:, 2:]
        Constants.details_record = details_record.iloc[:, 1:]
        # extracting all necessary info from details record
        Constants.stg_table = Constants.filepath.split('/')[-1][:-4]
        Constants.column_names = list(details_record['Column_Name'])
        Constants.column_length = list(details_record['File_Length'])
        Constants.column_type = list(details_record['DataType'])
        Constants.column_mandatory = list(details_record['Mandatory'])

    @staticmethod
    def transformation():
        '''
        Transformation of the objects while loading data into stg table.
        Covers date datatype Transformation
        '''
        # transforming datatype date inot DATE
        for i in range(len(Constants.column_names)):
            Constants.create_col_query.append(
                f'''{Constants.column_names[i]} {Constants.column_type[i]}{"("+str(Constants.column_length[i])+")" if Constants.column_type[i] != 'DATE' else ''} {'NOT NULL' if Constants.column_mandatory[i] == 'Y' else 'NULL'},''')
            try:
                if Constants.column_type[i] == 'DATE':
                    Constants.logger.info(
                        f'tranforming col datatype: {Constants.column_names[i]} into DATE')
                    Constants.stg_data.iloc[:, i] = pd.to_datetime(
                        Constants.stg_data.iloc[:, i], format="%Y%m%d")
            except:
                Constants.stg_data.iloc[:, i] = pd.to_datetime(
                    Constants.stg_data.iloc[:, i], format="%d%m%Y")

    @staticmethod
    def create_and_truncate_table(table_name):
        Constants.logger.info(f"creating table: {table_name}")
        query = f'''CREATE TABLE IF NOT EXISTS {Constants.schema}.{table_name} ({' '.join(Constants.create_col_query)[:-1]});'''
        Constants.curs.execute(query)
        Util.truncate(table_name)

    # Data Loading and all its Utilities goes here. Full load is the default load currently supported.
    @staticmethod
    def truncate(table_name):
        'Truncates and does Full load'
        Constants.curs.execute(
            f'''TRUNCATE TABLE {Constants.schema}.{table_name};''')
        Constants.logger.info(f'Truncate successfull: {table_name}')

    @staticmethod
    def stg_full_load(data, table_name):
        '''
        does full load for Intermediate table in the database.
        '''
        Constants.logger.info(
            f"Executing batch full load for table: {table_name}")
        # Creating a list of tupples from the dataframe values
        tpls = [tuple(x) for x in data.to_numpy()]
        # dataframe columns with Comma-separated
        cols = ','.join(list(map(lambda x: x.lower(), Constants.column_names)))
        # SQL query to execute
        formatting = '%s,'*len(Constants.column_names)
        sql = f"INSERT INTO {Constants.schema}.{table_name}({cols}) VALUES ({formatting[:-1]});"
        extras.execute_batch(Constants.curs, sql, tpls, Constants.page_size)
        Constants.logger.info("Full load successfull")

    @staticmethod
    def loading():
        '''
        loading objects in the country wise table after splitting
        '''
        Constants.logger.info(f'loading ...')
        Util.create_and_truncate_table(Constants.stg_table)
        Util.stg_full_load(Constants.stg_data, Constants.stg_table)
        Constants.logger.info("STG load successfull")
        Constants.logger.info(
            "####################### Going for Final ref Load for each country customers #######################")
        Constants.curs.execute(
            f'''select distinct country from {Constants.schema}.{Constants.stg_table};''')
        Constants.countries = [(country[0]).strip()
                               for country in Constants.curs.fetchall()]
        Constants.logger.info(
            f'All distinct countries present: {Constants.countries}')
        for countri in Constants.countries:
            Constants.logger.info(
                "--------------------------------------------")
            # creating table with schema
            country_name = 'Table_'+countri
            Util.create_and_truncate_table(country_name)
            # loading data from stg table
            Constants.logger.info(f'Inserting into table_{(countri.lower())}')
            countrywise_query = f'''INSERT INTO {Constants.schema}.{country_name} SELECT * FROM {Constants.schema}.{Constants.stg_table} WHERE UPPER(country) = '{countri}';'''
            Constants.curs.execute(countrywise_query)
