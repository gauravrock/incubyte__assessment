# Incubyte__assessment

Incubyte assessment data pipeline for data saturation of customers according to different countries for Multi-Specialty Hospital chain with locations all across the world.

Requirement for running script:
1. postgres db setup with 'demo' as schema.
2. adding creds to .ini file.
3. Running and adding lib using `pip install -r requirements.txt`

4.Running script with required 2 arguments example: 
`python Controller.py source_system/sample_file.txt source_system/detail.txt`
