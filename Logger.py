'''
default Logger for saving logs and printing logs to screen. 
Can be helpful for Debugging.
'''

import logging
import sys

def setup_custom_logger():
    formatter = logging.Formatter(fmt='%(asctime)s:  %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    screen = logging.Formatter(fmt='%(message)s',
                               datefmt='%Y-%m-%d %H:%M:%S')
    handler = logging.FileHandler('logs/etl.log', mode='a+')
    handler.setFormatter(formatter)
    screen_handler = logging.StreamHandler(stream=sys.stdout)
    screen_handler.setFormatter(screen)
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.addHandler(screen_handler)
    return logger
